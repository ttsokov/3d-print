Raspberry Pi 4 2U rack-mount bracket by russross on Thingiverse: https://www.thingiverse.com/thing:4078710

License - https://creativecommons.org/licenses/by-nc/4.0/

Changes made by me (ttsokov):
The original tray part with file `raspberry-pi-rack-tray.stl` is modified to have a fillment in the center and screw holes for mounting ESP32 hardware device. The modified part files are: [raspberry-pi-rack-tray-fill.stl](./files/raspberry-pi-rack-tray-fill.stl) and [raspberry-pi-rack-tray-fill.sldprt](./files/raspberry-pi-rack-tray-fill.sldprt).
